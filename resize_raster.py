import gdal

def resize_raster(raster_file, reference_raster_file, output_file):
    # Ouvrir les fichiers rasters avec GDAL
    raster = gdal.Open(raster_file)
    reference_raster = gdal.Open(reference_raster_file)

    # Récupérer les tailles des rasters
    raster_width = raster.RasterXSize
    raster_height = raster.RasterYSize
    reference_raster_width = reference_raster.RasterXSize
    reference_raster_height = reference_raster.RasterYSize

    # Calculer les nouveaux paramètres de géotransformation
    geotransform = reference_raster.GetGeoTransform()
    pixel_width = geotransform[1]
    pixel_height = geotransform[5]
    new_geotransform = (geotransform[0], pixel_width, 0, geotransform[3], 0, pixel_height)

    # Calculer les nouvelles tailles du raster
    new_raster_width = reference_raster.RasterXSize
    new_raster_height = reference_raster.RasterYSize
    print(new_raster_width ,new_raster_height )

    # Créer un nouveau fichier raster pour stocker le résultat
    driver = gdal.GetDriverByName("GTiff")
    result_raster = driver.Create(output_file, new_raster_width, new_raster_height, raster.RasterCount, raster.GetRasterBand(1).DataType)
    result_raster.SetProjection(reference_raster.GetProjection())
    result_raster.SetGeoTransform(new_geotransform)

    # Redimensionner le raster en utilisant la méthode de rééchantillonnage bilinéaire
    gdal.ReprojectImage(raster, result_raster, raster.GetProjection(), reference_raster.GetProjection(), gdal.GRA_Bilinear)

    # Fermer les fichiers rasters
    raster = None
    reference_raster = None
    result_raster = None

resize_raster('H:/atelier_geomatique/PyQGIS/route_export_raster_proximity_reclass_resampled.GTiff', 
              'H:/atelier_geomatique/PyQGIS/thefts_export_raster_reclass.GTiff',
              'H:/atelier_geomatique/PyQGIS/route_export_raster_proximity_reclass_resampled_resized.GTiff')
              
resize_raster('H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity_reclass_resampled.GTiff', 
              'H:/atelier_geomatique/PyQGIS/thefts_export_raster_reclass.GTiff',
              'H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity_reclass_resampled_resized.GTiff')
