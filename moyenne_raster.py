import os
import gdal
import numpy as np

def compute_raster_mean(input_raster1, input_raster2, input_raster3, output_raster):
    # Ouvrir les rasters d'entrée
    input_ds1 = gdal.Open(input_raster1)
    input_ds2 = gdal.Open(input_raster2)
    input_ds3 = gdal.Open(input_raster3)

    # Récupérer les dimensions et la géoréférence du raster d'entrée 1
    input_transform = input_ds1.GetGeoTransform()
    input_projection = input_ds1.GetProjection()
    input_width = input_ds1.RasterXSize
    input_height = input_ds1.RasterYSize

    # Lire les bandes des rasters d'entrée
    input_band1 = input_ds1.GetRasterBand(1)
    input_band2 = input_ds2.GetRasterBand(1)
    input_band3 = input_ds3.GetRasterBand(1)

    # Lire les valeurs des pixels de chaque bande en tant que tableau numpy
    data1 = input_band1.ReadAsArray()
    data2 = input_band2.ReadAsArray()
    data3 = input_band3.ReadAsArray()

    # Calculer la moyenne des valeurs des pixels pour chaque emplacement de pixel
    mean_data = np.mean([data1, data2, data3], axis=0)

    # Créer le nouveau raster de sortie en utilisant GDAL
    driver = gdal.GetDriverByName("GTiff")
    output_ds = driver.Create(output_raster, input_width, input_height, 1, gdal.GDT_Float32)

    # Écrire les données de moyenne dans la bande unique du nouveau raster
    output_ds.GetRasterBand(1).WriteArray(mean_data)

    # Assigner la géoréférence et la projection du raster d'entrée 1 au nouveau raster
    output_ds.SetGeoTransform(input_transform)
    output_ds.SetProjection(input_projection)

    # Fermer les rasters d'entrée et de sortie
    input_ds1 = None
    input_ds2 = None
    input_ds3 = None
    output_ds = None

compute_raster_mean(
'H:/atelier_geomatique/PyQGIS/route_export_raster_proximity_reclass_resampled_resized.GTiff', 
'H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity_reclass_resampled_resized.GTiff', 
'H:/atelier_geomatique/PyQGIS/thefts_export_raster_reclass.GTiff', 
'H:/atelier_geomatique/PyQGIS/spatial_thoughts.GTiff')