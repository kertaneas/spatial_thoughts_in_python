from osgeo import gdal
from math import *
 

def resample_raster(input_path, output_path, target_resolution=100):

    # Open the input raster
    input_raster = gdal.Open(input_path)

    # Get the input raster size and geotransform 
    rows = int(input_raster.RasterYSize/10) #(on divise par 10 car on multiplie la taille des pixels par 10)
    cols = int(input_raster.RasterXSize/10) #(on divise par 10 car on multiplie la taille des pixels par 10)
    geotransform = input_raster.GetGeoTransform()

    # Define the target geotransform
    target_geotransform = (geotransform[0], target_resolution, geotransform[2], geotransform[3], geotransform[4], -target_resolution)

    # Define the output raster
    driver = gdal.GetDriverByName('GTiff')
    output_raster = driver.Create(output_path, cols, rows, 1, gdal.GDT_Int16)
    output_raster.SetGeoTransform(target_geotransform)
    output_raster.SetProjection(input_raster.GetProjection())

    # Resample the raster
    gdal.ReprojectImage(input_raster, output_raster, input_raster.GetProjection(),
                        output_raster.GetProjection(), gdal.GRA_Average)

    # Close the input and output rasters
    input_raster = None
    output_raster = None

resample_raster('H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity_reclass.GTiff', 'H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity_reclass_resampled.GTiff', target_resolution=100)
resample_raster('H:/atelier_geomatique/PyQGIS/route_export_raster_proximity_reclass.GTiff', 'H:/atelier_geomatique/PyQGIS/route_export_raster_proximity_reclass_resampled.GTiff', target_resolution=100)