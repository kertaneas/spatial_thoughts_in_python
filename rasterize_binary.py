import os
from osgeo import gdal, ogr

def rasterize_shapefile(input_shapefile, output_raster, pixel_size):
    # Ouvrir le shapefile et obtenir la couche
    shapefile = ogr.Open(input_shapefile)
    layer = shapefile.GetLayer()

    # Obtenir les informations de projection de la couche
    srs = layer.GetSpatialRef()
    srs_wkt = srs.ExportToWkt()

    # Déterminer les dimensions de la sortie en pixels
    extent = layer.GetExtent()
    x_min, x_max, y_min, y_max = extent
    cols = int((x_max - x_min) / pixel_size)
    rows = int((y_max - y_min) / pixel_size)

    # Créer un raster vide avec les dimensions et la projection de la couche
    driver = gdal.GetDriverByName("GTiff")
    output_raster = driver.Create(output_raster, cols, rows, 1, gdal.GDT_Byte)
    output_raster.SetProjection(srs_wkt)
    output_raster.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))

    # Rasterize la couche du shapefile sur le raster
    gdal.RasterizeLayer(output_raster, [1], layer, burn_values=[1])

    # Fermer les fichiers et libérer la mémoire
    output_raster = None
    shapefile = None

rasterize_shapefile('H:/atelier_geomatique/PyQGIS/parking_export.shp', 'H:/atelier_geomatique/PyQGIS/parking_export_raster.GTiff', 10)
rasterize_shapefile('H:/atelier_geomatique/PyQGIS/route_export.shp', 'H:/atelier_geomatique/PyQGIS/route_export_raster.GTiff', 10)