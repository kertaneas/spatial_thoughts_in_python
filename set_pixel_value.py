import numpy as np
import gdal

def set_pixel_value(raster_path):
    # Ouvrir le raster
    raster = gdal.Open(raster_path, gdal.GA_Update)
    band = raster.GetRasterBand(1)
    
    # Lire les données de la bande en tant qu'array numpy
    data = band.ReadAsArray()
    
    # Affecter la valeur 10 aux pixels qui sont égaux à 0
    data[data == 0] = 10
    
    # Écrire les nouvelles données dans la bande
    band.WriteArray(data)
    
    # Fermer le raster
    raster = None

set_pixel_value('H:/atelier_geomatique/PyQGIS/route_export_raster_proximity_reclass_resampled_resized.GTiff')


def set_pixel_value2(raster_path):
    # Ouvrir le raster
    raster = gdal.Open(raster_path, gdal.GA_Update)
    band = raster.GetRasterBand(1)
    
    # Lire les données de la bande en tant qu'array numpy
    data = band.ReadAsArray()
    
    # Affecter la valeur 10 aux pixels qui sont égaux à 0
    data[data == 0] = 100
    
    # Écrire les nouvelles données dans la bande
    band.WriteArray(data)
    
    # Fermer le raster
    raster = None

set_pixel_value2('H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity_reclass_resampled_resized.GTiff')
