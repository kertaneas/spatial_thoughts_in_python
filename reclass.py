import os
import numpy as np
from osgeo import gdal
def reclassify_raster(input_path, output_path):
    # Load input raster
    input_raster = gdal.Open(input_path)

    # Get input raster metadata
    x_size = input_raster.RasterXSize
    y_size = input_raster.RasterYSize
    projection = input_raster.GetProjection()
    geotransform = input_raster.GetGeoTransform()

    # Read input raster as array
    input_array = input_raster.ReadAsArray()

    # Reclassify input array
    output_array = np.zeros_like(input_array)
    output_array[input_array <= 50] = 100
    output_array[(input_array > 50) & (input_array <= 100)] = 50
    output_array[input_array > 100] = 10

    # Create output raster
    output_driver = gdal.GetDriverByName('GTiff')
    output_raster = output_driver.Create(output_path, x_size, y_size, 1, gdal.GDT_Int16)
    output_raster.SetProjection(projection)
    output_raster.SetGeoTransform(geotransform)

    # Write output array to output raster
    output_band = output_raster.GetRasterBand(1)
    output_band.WriteArray(output_array)
    output_band.SetNoDataValue(0)
    output_band.FlushCache()

    # Close input and output rasters
    input_raster = None
    output_raster = None

reclassify_raster("H:/atelier_geomatique/PyQGIS/route_export_raster_proximity.GTiff", "H:/atelier_geomatique/PyQGIS/route_export_raster_proximity_reclass.GTiff")

def reclassify_raster2(input_path, output_path):
    # Load input raster
    input_raster = gdal.Open(input_path)

    # Get input raster metadata
    x_size = input_raster.RasterXSize
    y_size = input_raster.RasterYSize
    projection = input_raster.GetProjection()
    geotransform = input_raster.GetGeoTransform()

    # Read input raster as array
    input_array = input_raster.ReadAsArray()

    # Reclassify input array
    output_array = np.zeros_like(input_array)
    output_array[input_array <= 50] = 10
    output_array[(input_array > 50) & (input_array <= 100)] = 50
    output_array[input_array > 100] = 100

    # Create output raster
    output_driver = gdal.GetDriverByName('GTiff')
    output_raster = output_driver.Create(output_path, x_size, y_size, 1, gdal.GDT_Int16)
    output_raster.SetProjection(projection)
    output_raster.SetGeoTransform(geotransform)

    # Write output array to output raster
    output_band = output_raster.GetRasterBand(1)
    output_band.WriteArray(output_array)
    output_band.SetNoDataValue(0)
    output_band.FlushCache()

    # Close input and output rasters
    input_raster = None
    output_raster = None

reclassify_raster2("H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity.GTiff", "H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity_reclass.GTiff")

def reclassify_raster3(input_path, output_path):
    # Load input raster
    input_raster = gdal.Open(input_path)

    # Get input raster metadata
    x_size = input_raster.RasterXSize
    y_size = input_raster.RasterYSize
    projection = input_raster.GetProjection()
    geotransform = input_raster.GetGeoTransform()

    # Read input raster as array
    input_array = input_raster.ReadAsArray()

    # Reclassify input array
    output_array = np.zeros_like(input_array)
    output_array[input_array <= 10] = 10
    output_array[(input_array > 10) & (input_array <= 20)] = 50
    output_array[input_array > 20] = 100

    # Create output raster
    output_driver = gdal.GetDriverByName('GTiff')
    output_raster = output_driver.Create(output_path, x_size, y_size, 1, gdal.GDT_Int16)
    output_raster.SetProjection(projection)
    output_raster.SetGeoTransform(geotransform)

    # Write output array to output raster
    output_band = output_raster.GetRasterBand(1)
    output_band.WriteArray(output_array)
    output_band.SetNoDataValue(0)
    output_band.FlushCache()

    # Close input and output rasters
    input_raster = None
    output_raster = None

reclassify_raster3("H:/atelier_geomatique/PyQGIS/thefts_export_raster.tif", "H:/atelier_geomatique/PyQGIS/thefts_export_raster_reclass.GTiff")
