import gdal
import osr

def compute_proximity(input_raster_path, output_raster_path):
    
    # ouvrir le fichier raster d'entrée
    input_raster = gdal.Open(input_raster_path)
    
    # récupérer les dimensions du raster
    x_size = input_raster.RasterXSize
    y_size = input_raster.RasterYSize
    
    # définir la projection du raster de sortie
    output_projection = osr.SpatialReference()
    output_projection.ImportFromWkt(input_raster.GetProjection())
    
    # créer le fichier raster de sortie
    driver = gdal.GetDriverByName('GTiff')
    output_raster = driver.Create(output_raster_path, x_size, y_size, 1, gdal.GDT_Int16)
    output_raster.SetProjection(output_projection.ExportToWkt())
    output_raster.SetGeoTransform(input_raster.GetGeoTransform())
    
    # calculer le raster de proximité
    gdal.ComputeProximity(input_raster.GetRasterBand(1), output_raster.GetRasterBand(1), options=['VALUES=1', 'DISTUNITS=GEO'])
    
    # fermer les fichiers rasters
    input_raster = None
    output_raster = None

compute_proximity("H:/atelier_geomatique/PyQGIS/route_export_raster.GTiff", "H:/atelier_geomatique/PyQGIS/route_export_raster_proximity.GTiff")
compute_proximity("H:/atelier_geomatique/PyQGIS/parking_export_raster.GTiff", "H:/atelier_geomatique/PyQGIS/parking_export_raster_proximity.GTiff")
